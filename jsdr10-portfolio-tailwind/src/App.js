import { Routes, Route } from 'react-router-dom'
import './App.css';
import Layout from './components/Layout'
import Home from './components/Home'
import About from './components/About'
import Portfolio from './components/Portfolio'
import Contact from './components/Contact'
import Dashboard from './components/Dashboard'
import NotFound404 from './components/NotFound404'

function App() {
  return (
    <div className="App">
      <Routes>

        <Route path="/" element={<Layout />}>

          <Route index element={<Home />}/>
          <Route path="/about" element={<About />}/>
          <Route path="/portfolio" element={<Portfolio />}/>
          <Route path="/contact" element={<Contact />}/>
          <Route path="/dashboard" element={<Dashboard />}/>

          <Route path="*" element={<NotFound404 />}/>

        </Route>

      </Routes>
    </div>
  );
}

export default App;