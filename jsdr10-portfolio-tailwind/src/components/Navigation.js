import { Link, NavLink } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faIgloo, faAddressCard, faBookSkull, faEnvelopeOpenText, faUser } from '@fortawesome/free-solid-svg-icons'
import { faDev } from '@fortawesome/free-brands-svg-icons'
import Socials from './Socials'

const Navigation = () => {
    return(
       <div className="nav-bar w-14 h-[100vh] bg-black fixed top-0 left-0 p-3 flex flex-col justify-center">

            <Link className='logo mb-auto' to="/">
                <FontAwesomeIcon icon={faDev} className="text-yellow-400 text-4xl w-full"/>
            </Link>
            
            <nav className="mb-auto">
                <NavLink exact="true" activeclassname="active" to="/">
                    <FontAwesomeIcon icon={faIgloo} className="text-white hover:text-yellow-400 my-4 text-2xl w-full"/>
                </NavLink>

                <NavLink exact="true" activeclassname="active" to="/about" >
                    <FontAwesomeIcon icon={faAddressCard} className="text-white hover:text-yellow-400 my-4 text-2xl w-full"/>
                </NavLink>

                <NavLink exact="true" activeclassname="active" to="/portfolio">
                    <FontAwesomeIcon icon={faBookSkull} className="text-white hover:text-yellow-400 my-4 text-2xl w-full"/>
                </NavLink>

                <NavLink exact="true" activeclassname="active" to="/contact">
                    <FontAwesomeIcon icon={faEnvelopeOpenText} className="text-white hover:text-yellow-400 my-4 text-2xl w-full"/>
                </NavLink>

                <NavLink exact="true" activeclassname="active" to="/dashboard">
                    <FontAwesomeIcon icon={faUser} className="text-white hover:text-yellow-400 my-4 text-2xl w-full"/>
                </NavLink>
            </nav>

            <Socials />

       </div>
    )
}

export default Navigation