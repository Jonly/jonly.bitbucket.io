import AnimatedLetters from "./AnimatedLetters"

const Contact = () => {
    return(
        <div className="contact-page">
            <AnimatedLetters strArray={'Contact'.split('')}/>
        </div>
    )
}

export default Contact