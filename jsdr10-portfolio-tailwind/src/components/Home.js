import { Link } from 'react-router-dom'
import AnimatedLetters from "./AnimatedLetters"

const Home = () => {
    const hiArray = 'Hi,'.split('');
    const imArray = "I'm".split('');
    const nameArray = 'Young'.split('');
    const jobArray = 'web developer.'.split('');

    return(
        <div className="home-page h-[calc(100vh-40px)] flex items-center pl-10">
            <div className='mb-[130px]'>
                <h1 className='mb-5'>
                    <AnimatedLetters strArray={hiArray} startIdx={11} />
                    <br />
                    <AnimatedLetters strArray={imArray} startIdx={14} />
                    <div className="name-logo text-6xl"><span>Jay</span></div>
                    <AnimatedLetters strArray={nameArray} startIdx={17} />
                    <br />
                    <AnimatedLetters strArray={jobArray} startIdx={22} />
                </h1>
                <h2 className='mb-5'>Frontend Developer / Javascript Expert / Teacher</h2>
                <Link to="/contact">
                    <span className="cursor-pointer inline-block px-4 py-2 bg-brand-gold text-black hover:bg-black hover:text-brand-gold">Contact me</span>
                </Link>
            </div>

            {/* <div className="row flex flex-wrap -mx-3">

                <div className="col px-3 pb-6 w-full md:w-1/2">
                    <img src="https://picsum.photos/id/10/1000/800" alt=""/>
                </div>

                <div className="col px-3 pb-6 w-full md:w-1/2">
                    <img src="https://picsum.photos/id/11/1000/800" alt=""/>
                </div>

            </div>

            <div className="row flex flex-wrap -mx-3">

                <div className="col px-3 pb-6 w-full md:w-1/2">
                    <img src="https://picsum.photos/id/12/1000/800" alt=""/>
                </div>

                <div className="col px-3 pb-6 w-full md:w-1/2">
                    <img src="https://picsum.photos/id/13/1000/800" alt=""/>
                </div>

            </div> */}

        </div>
    )
}

export default Home