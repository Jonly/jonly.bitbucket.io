import { useState } from 'react'
import { signInWithEmail } from '../firebase'
// registerNewUser, 
import AnimatedLetters from './AnimatedLetters'

const Login = () => {

    let [email, setEmail] = useState('jay.young@generalassemb.ly')
    let [password, setPassword] = useState('login123')

    const stringTitle = 'Login'



    function updateEmail(event) {
        setEmail(event.target.value)
        console.log(email)
    }

    function updatePassword(event) {
        setPassword(event.target.value)
        console.log(password)
    }

    function logUserIn(event) {
        event.preventDefault()
        console.log(email, password)
        setPassword('')
        setEmail('')

        signInWithEmail(email, password)
    }

    // function registerUser(event) {
    //     event.preventDefault()
    //     console.log(email, password)
    //     setPassword('')
    //     setEmail('')

    //     registerNewUser(email, password)
    // }


    return(
        <div className="login-page">
            <AnimatedLetters strArray={stringTitle.split('')} customData={stringTitle}/>

            <div className='forms w-60'>
                <form className='login my-5'>
                    <h3 className='mb-4'>Login form</h3>
                    <input className='mr-2 text-black mb-4 w-full' type='text' placeholder="Email" value={email} onChange={updateEmail}/>
                    <input className='mr-2 text-black mb-4 w-full' type='password' placeholder="Password" value={password} onChange={updatePassword}/>
                    <input className="mt-4 bg-gray-400 px-3 mb-4 w-full" type="submit" value="Submit" onClick={logUserIn}/>
                </form>

                {/* <form className='register my-5'>
                    <h3 className='mb-4'>Register form</h3>
                    <input className='mr-2 text-black mb-4 w-full' type='text' placeholder="Email" value={email} onChange={updateEmail}/>
                    <input className='mr-2 text-black mb-4 w-full' type='password' placeholder="Password" value={password} onChange={updatePassword}/>
                    <input className="mt-4 bg-gray-400 px-3 mb-4 w-full" type="submit" value="Submit" onClick={registerUser}/>
                </form> */}
            </div>
        </div>
    )
}

export default Login