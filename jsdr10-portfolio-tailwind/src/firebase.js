import { initializeApp } from 'firebase/app'
import { getAuth, signInWithEmailAndPassword, createUserWithEmailAndPassword, signOut } from 'firebase/auth'

import { getStorage } from 'firebase/storage'
import { getFirestore, collection } from 'firebase/firestore'

const firebaseConfig = {
  apiKey: "AIzaSyAqq79VSd_D9ajAA-sGtQXu9CMh_Rurrt0",
  authDomain: "jsdr10-portfolio-f1f87.firebaseapp.com",
  projectId: "jsdr10-portfolio-f1f87",
  storageBucket: "jsdr10-portfolio-f1f87.appspot.com",
  messagingSenderId: "756977018863",
  appId: "1:756977018863:web:dd0130a52c8b3e816df81a"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig)
const auth = getAuth()

const db = getFirestore(app)

export const storage = getStorage(app)
export const dbRef = collection(db, 'portfolio')

export const registerNewUser = (email, password) => createUserWithEmailAndPassword(auth, email, password)
.then((userCredential) => {
    const user = userCredential.user
    console.log(user)
})
.catch((error) => {
    console.log(error.code)
    console.log(error.message)
})

export const signInWithEmail = (email, password) => signInWithEmailAndPassword(auth, email, password)
.then((userCredential) => {
    const user = userCredential.user
    console.log(user)
})
.catch((error) => {
    console.log(error.code)
    console.log(error.message)
})

// export const signInWithEmail = (email, password) => {
//     console.log('test', email, password)
// }

export const signOutUser = () => signOut(auth)
.then(() => {
    console.log('Signed out')
})
.catch((error) => {
    console.log(error.code)
    console.log(error.message)
})


