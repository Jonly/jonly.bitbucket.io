import AnimatedLetters from './AnimatedLetters'

const About = () => {
    return(
        <div className="about-page">
            <AnimatedLetters strArray={'About'.split('')} customData={'about'} />
        </div>
    )
}

export default About