import { signOutUser, dbRef, storage } from '../firebase'
import { ref, uploadBytes, getDownloadURL } from 'firebase/storage'
import { addDoc } from 'firebase/firestore'
import { useRef, useState } from 'react'
import AnimatedLetters from './AnimatedLetters'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'

const Account = () => {

    const stringTitle = 'Account'

    const form = useRef([])

    // Create variables for disabling form fields and outputting progress or errors to screen
    const [progress, setProgress] = useState('')
    const [disabled, setDisabled] = useState(false)

    // Submit image and upload to storage
    const submitPortfolio = (event) => {
        event.preventDefault()
        console.log('Submitting')

        setDisabled(true)
        setProgress('') // targets data-progress div

        console.log(form.current)

        const image = form.current[0].files[0]
        console.log(image);

        // Upload the image
        const storageRef = ref(storage, `portfolio/${image.name}`)

        setProgress('Uploading image...')

        // Wait for return URL 
        uploadBytes(storageRef, image)
            .then((snapshot) => {
                console.log(snapshot.ref)
                getImageURLFromUpload(snapshot.ref)
            })
            .catch((error) => {
                setProgress('Storage ERROR: '+error)
                console.log(error)
            })
    }

    // Get the image URL from the storage
    const getImageURLFromUpload = (snapRef) => {
        getDownloadURL(snapRef)
            .then((downloadURL) => {
                console.log(downloadURL)
                savePortfolioToDatabase(downloadURL)
            })
            .catch((error) => {
                console.log(error)
                setProgress('Storage ERROR: '+error)
                setDisabled(false)
            })
    }

    // Save all data including the image URL to the database
    const savePortfolioToDatabase = (imageURL) => {
        const portfolioItem = {
            imageURL: imageURL,
            itemName: form.current[1].value,
            itemDescription: form.current[2].value,
            itemLink: form.current[3].value
        }

        setProgress('Saving data...')

        addDoc(dbRef, portfolioItem)
            .then((documentRef) => {
                console.log('Document has been created', documentRef);
                form.current.reset()
                setDisabled(false)
                setProgress('')
            })
            .catch((error) => {
                console.log(error)
                setProgress('Database ERROR: '+error)
                setDisabled(false)
            })
    }

    return(
        <>
            <AnimatedLetters strArray={stringTitle.split('')} customData={stringTitle}/>
            

            <form className='my-5 flex flex-col w-60' ref={form} onSubmit={submitPortfolio}>
                <input disabled={disabled} type='file' className='mb-4'/>
                <input disabled={disabled} type='text' placeholder='Project Name' className='mb-4 text-black'/>
                <input disabled={disabled} type='text' placeholder='Description' className='mb-4 text-black'/>
                <input disabled={disabled} type='text' placeholder='Link URL' className='mb-4 text-black'/>

                <button disabled={disabled} type="submit" className="mt-4 bg-gray-400 px-3">Submit</button>
            </form>

            <div className='data-progress w-60'>
                <FontAwesomeIcon icon={faSpinner} className={`${disabled ? 'inline' : 'hidden'} animate-spin w-5`}/>
                <span className='ml-3'>{ progress }</span>
            </div>


            <button className="mt-20 bg-gray-400 px-3 w-60" onClick={signOutUser}>Logout</button>
        </>
    )
}

export default Account