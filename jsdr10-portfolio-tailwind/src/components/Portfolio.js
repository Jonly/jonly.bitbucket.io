import { useEffect, useState } from 'react'
import { dbRef } from '../firebase'
import { getDocs } from 'firebase/firestore'
import AnimatedLetters from './AnimatedLetters'


const Portfolio = () => {
    const [portfolioData, setPorfolioData] = useState([])

    const stringTitle = 'Portfolio'

    useEffect(() => {
        getPortfolioData()
    }, [])


    const getPortfolioData = async () => {
        const querySnapshot = await getDocs(dbRef)
        console.log(querySnapshot)
        setPorfolioData( querySnapshot.docs.map( (doc) => doc.data() ) )
    }


    const renderPortfolio = (portfolio) => {
        return(
            <>
                {
                    portfolio.map((port, idx) => {
                        return(
                            <div key={idx} className={`card-${idx} card w-full md:w-1/3 px-5 mb-5`}>
                                <div className="bg-[#555] h-full flex flex-col p-5">
                                    <img src={port.imageURL} alt={port.itemName} className="w-full h-80 object-cover"/>
                                    <h3 className='text-white text-2xl font-bold my-2'>{ port.itemName }</h3>
                                    <p className='mb-4'>{ port.itemDescription }</p>
                                    <button onClick={ () => window.open(port.itemLink) } className='bg-[#444] hover:bg-[#222] px-4 mt-auto'>Read More</button>
                                </div>
                            </div>
                        )
                    })
                }
            </>
        )
    }

    
    return(
        <div className="portfolio-page">
            <AnimatedLetters strArray={stringTitle.split('')} customData={stringTitle}/>

            <div className="portfolio-loop my-5 flex flex-wrap -mx-5">

                { renderPortfolio(portfolioData) }

            </div>

        </div>
    )
}

export default Portfolio