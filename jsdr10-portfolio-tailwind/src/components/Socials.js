import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLinkedin, faTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons'

const Socials = () => {
    return (
        <ul>
            <li>
                <a href="https://linkedin.com" target="_blank" rel="noreferrer">
                    <FontAwesomeIcon icon={faLinkedin} className="text-white hover:text-yellow-400 my-4 w-full"/>
                </a>
            </li>
            <li>
                <a href="https://twitter.com" target="_blank" rel="noreferrer">
                    <FontAwesomeIcon icon={faTwitter} className="text-white hover:text-yellow-400 my-4 w-full"/>
                </a>
            </li>
            <li>
                <a href="https://instagram.com" target="_blank" rel="noreferrer">
                    <FontAwesomeIcon icon={faInstagram} className="text-white hover:text-yellow-400 my-4 w-full"/>
                </a>
            </li>
        </ul>
    )
}

export default Socials